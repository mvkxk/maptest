
import Foundation
import MapKit

struct Location: Identifiable, Equatable {
    var id: Int
    var name: String
    var coordinates: CLLocationCoordinate2D
    var imageName: String
    var date: String
    var time: String
    var type: String
    
    static func == (lhs: Location, rhs: Location) -> Bool {
        return lhs.id == rhs.id
    }
}

