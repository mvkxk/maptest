
import Foundation
import MapKit
import SwiftUI

final class LocationViewModel: NSObject, ObservableObject {
    
    private var locationManager: CLLocationManager? = CLLocationManager()
    
    @Published var userCoordinates: CLLocationCoordinate2D {
        didSet {
            locations[0].coordinates = userCoordinates
        }
    }
    @Published var locations: [Location]
    @Published var mapLocation: Location {
        didSet {
            updateMapRegion(location: mapLocation)
        }
    }
    @Published var isShowAlert: Bool = false
    @Published var alertMessage: String = ""
    
    @Published var mapRegion: MKCoordinateRegion = MKCoordinateRegion()
    
    private var mapSpan = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
    
    override init() {
        locations = MockDataService.locations
        mapLocation = MockDataService.locations.first!
        userCoordinates = locationManager?.location?.coordinate ?? MockDataService.locations.first!.coordinates
        super.init()
        setupLocationService()
        locations[0].coordinates = userCoordinates
        updateMapRegion(location: locations.first!)
    }
    
    private func updateMapRegion(location: Location) {
        withAnimation(.easeInOut) {
            mapRegion = MKCoordinateRegion(
                center: location.coordinates,
                span: mapSpan)
        }
    }
    
    private func setupLocationService() {
        DispatchQueue.global().async { [weak self] in
            if CLLocationManager.locationServicesEnabled() {
                self?.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
                self?.locationManager?.delegate = self
            } else {
                self?.alertMessage = "Ваша местоположение не доступно"
                return
            }
        }
    }
    
    func showNextLocation(location: Location) {
        mapLocation = location
    }
    
    func nextLocation() {
        guard let currentIndex = locations.firstIndex( where: { $0 == mapLocation }) else { return }
        
        let nextIndex = currentIndex + 1
        guard locations.indices.contains(nextIndex) else {
            guard let firstLocation = locations.first else { return }
            showNextLocation(location: firstLocation)
            return
        }
        
        let nextLocation = locations[nextIndex]
        showNextLocation(location: nextLocation)
    }
    
}

extension LocationViewModel: CLLocationManagerDelegate {
    
    func checkLocationAuthorication() {
        if let locationManager = locationManager {
            switch locationManager.authorizationStatus {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .restricted:
                alertMessage = "Местоположение недоступно из-за настроек приватности"
            case .denied:
                alertMessage = "Вы запретили использовать ваше местоположение, для полной функциональности пожалуйста разрешите использование местоположения в настройках"
            case .authorizedAlways, .authorizedWhenInUse:
                userCoordinates = locationManager.location!.coordinate
            @unknown default:
                break
            }
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorication()
    }
}
