
import Foundation
import MapKit

final class MockDataService {
    static let locations: [Location] = [
        Location(
            id: 0,
            name: "user",
            coordinates: CLLocationCoordinate2D(latitude: 59.946689, longitude: 30.332592),
            imageName: "icon_my_tracker",
            date: "02.07.17",
            time: "14:00",
            type: "GPS"),
        Location(
            id: 1,
            name: "Вася",
            coordinates: CLLocationCoordinate2D(latitude: 59.961324, longitude: 30.315857),
            imageName: "boy",
            date: "02.07.17",
            time: "14:00",
            type: "GPS"),
        Location(
            id: 2,
            name: "Коля",
            coordinates: CLLocationCoordinate2D(latitude: 59.940129, longitude: 30.311174),
            imageName: "man",
            date: "02.07.17",
            time: "14:00",
            type: "GPS"),
        Location(
            id: 3,
            name: "Даша",
            coordinates: CLLocationCoordinate2D(latitude: 59.944697, longitude: 30.368817),
            imageName: "girl",
            date: "02.07.17",
            time: "14:00",
            type: "GPS")
    ]
}
