
import SwiftUI

struct LocationPinView: View {
    
    let location: Location
    var isSelected: Bool = false
    
    var body: some View {
        VStack {
            ZStack {
                Image("icon_tracker")
                    .resizable()
                    .scaledToFill()
                    .frame(width: 70, height: 70)
                
                Image(location.imageName)
                    .resizable()
                    .scaledToFill()
                    .frame(width: 45, height: 45)
                    .cornerRadius(50)
                    .offset(y: -5)
            }
            
            if isSelected {
                VStack {
                    
                    Text(location.name)
                        .font(.system(size: 12))
                        .frame(width: 80, alignment: .leading)
                    
                    HStack(spacing: 3){
                        
                        Text(location.type + ",")
                            .font(.system(size: 14))
                        
                        Text(location.time)
                            .font(.system(size: 14))
                    }
                    .foregroundColor(.gray)
                    .frame(width: 80, alignment: .leading)
                }
                .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                .background(.white)
                .cornerRadius(30)
                .offset(x:65, y: -20)
            }
        }
    }
}

struct LocationPinView_Previews: PreviewProvider {
    static var previews: some View {
        LocationPinView(location: MockDataService.locations.first!)
    }
}
