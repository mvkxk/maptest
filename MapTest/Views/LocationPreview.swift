
import SwiftUI

struct LocationPreview: View {
    
    let location: Location
    
    var body: some View {
        if location.id != 0 {
            HStack(spacing: 16) {
                
                Image(location.imageName)
                    .resizable()
                    .frame(width: 70, height: 70, alignment: .center)
                    .cornerRadius(50)
                    .background(
                        Circle()
                            .stroke(.blue, lineWidth: 4)
                    )
                    .padding(.bottom, 25)
                
                VStack(spacing: 5) {
                    Text(location.name)
                        .font(.system(size: 20, weight: .medium))
                        .frame(maxWidth: .infinity, alignment: .leading)
                    
                    HStack(spacing: 10) {
                        
                        HStack(spacing: 2) {
                            Image(systemName: "wifi")
                                .foregroundColor(Color.blue)
                            
                            Text(location.type)
                        }
                        
                        HStack(spacing: 2) {
                            Image(systemName: "calendar")
                                .foregroundColor(Color.blue)
                            
                            Text(location.date)
                        }
                        
                        HStack(spacing: 2) {
                            Image(systemName: "clock")
                                .foregroundColor(Color.blue)
                            
                            Text(location.time)
                        }
                    }
                    .font(.system(size: 16))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    
                    Button(action: {
                        
                    }, label: {
                        Text("Посмотреть историю")
                            .foregroundColor(.white)
                            .font(.system(size: 14, weight: .semibold))
                            .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
                            .background(.blue)
                            .cornerRadius(18)
                            .padding(.top, 15)
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.leading, 20)
                    })
                }
                
                
            }.padding(.horizontal, 20)
                .padding(.vertical, 15)
                .background(.white)
        }
    }
}

struct LocationPreview_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.green.ignoresSafeArea()
            
            LocationPreview(location: MockDataService.locations.first!)
        }
        
    }
}
