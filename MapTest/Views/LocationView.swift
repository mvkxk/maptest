
import SwiftUI
import MapKit

struct LocationView: View {
    
    @EnvironmentObject private var locationViewModel: LocationViewModel
    
    var body: some View {
        ZStack {
            
            Map(coordinateRegion: $locationViewModel.mapRegion,
                showsUserLocation: false,
                annotationItems: locationViewModel.locations,
                annotationContent: { location in
                MapAnnotation(coordinate: location.coordinates) {
                    if location.id == 0 {
                        Image(location.imageName)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 50, height: 50)
                            .onTapGesture {
                                locationViewModel.showNextLocation(location: location)
                            }
                    } else {
                        LocationPinView(location: location,
                                        isSelected: locationViewModel.mapLocation == location)
                        .scaleEffect(locationViewModel.mapLocation == location ? 1 : 0.7)
                        .shadow(radius: 10)
                        .onTapGesture {
                            locationViewModel.showNextLocation(location: location)
                        }
                    }
                }
            })
            .ignoresSafeArea()
            
            VStack(spacing: 10) {
                Button(action: {
                    withAnimation(.easeInOut) {
                        locationViewModel.mapRegion.span.longitudeDelta /= 2
                        locationViewModel.mapRegion.span.latitudeDelta /= 2
                    }
                }, label: {
                    Image("icon_zoom_plus")
                })
                
                Button(action: {
                    withAnimation(.easeInOut) {
                        locationViewModel.mapRegion.span.longitudeDelta *= 2
                        locationViewModel.mapRegion.span.latitudeDelta *= 2
                    }
                }, label: {
                    Image("icon_zoom_minus")
                })
                
                Button(action: {
                    locationViewModel.showNextLocation(location: locationViewModel.locations[0])
                }, label: {
                    Image("icon_mylocation")
                })
                
                Button(action: {
                    locationViewModel.nextLocation()
                }, label: {
                    Image("icon_next_tracker")
                })
            }.frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
                .padding(.trailing, 5)
            
            LocationPreview(location: locationViewModel.mapLocation)
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
                .padding(.bottom, 10)
                .ignoresSafeArea()
        }
        .onAppear {
            locationViewModel.checkLocationAuthorication()
        }
        .alert(isPresented: $locationViewModel.isShowAlert, content: {
            Alert(title: Text(locationViewModel.alertMessage), dismissButton: .cancel())
        })
    }
}

struct LocationView_Previews: PreviewProvider {
    static var previews: some View {
        LocationView()
            .environmentObject(LocationViewModel())
    }
}
